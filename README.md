# Readme for ansible-dehydrated

To issue certificates for your role include dehydrated in meta/main.yml and set `dehydrated_deploy` and `dehydrated_certs` variables.

Structure of `dehydrated_deploy`:
```
  - domain: "{{ ansible_facts.fqdn }}"
    key:
      - path: /etc/ssl/private/key.pem
        owner: root
    cert:
      - path: /etc/ssl/private/cert.pem
        owner: root
    fullchain:
      - path: /etc/ssl/private/fullchain.pem
        owner: root
    chain:
      - path: /etc/ssl/private/chain.pem
        owner: root
    reload_cmd: optional

```

Structure of `dehydrated_certs`:
```
- domains:
  - domain1
  - domain2
  - domain3
  alias: optional
```


These two variables have to be set by every role including it in dependencies, such that previous values are overwritten.
